module.exports = function(io) {
    var userNames = [];

    io.on("connection", function(socket) {
        console.log("connected");

        socket.sinkOrSwim = {
            userName: "User" + Math.round(Math.random() * 10000 +  10000)
        };

        socket.emit("connected", {
            userName: socket.sinkOrSwim.userName
        });

        socket.on("send_message", function(data) {
            var _date         = new Date();
            data["timestamp"] = _date;
            io.emit("broadcast_message", data);
        });

        socket.on("newUserName", function(newName) {
            if(socket.sinkOrSwim.userName === newName) {
                io.emit("addUser", newName);
                userNames.push(socket);
            } else {
                io.emit("changeName", socket.sinkOrSwim.userName, newName);
            }
            socket.sinkOrSwim.userName = newName;
        });

        socket.on("error", function(error) {
            console.error("socketError:", error);
        });

        socket.on("userLeaving", function() {
            console.log("leaving:", socket.sinkOrSwim);
            io.emit("deleteUser", socket.sinkOrSwim.userName);
        });

        socket.on('disconnect', function() {
            //io.emit("connected", user_connected);
        });
    });
};