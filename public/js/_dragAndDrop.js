"use strict";

/**
 *
 * @type {{loadModules}}
 */
sinkOrSwim.dragAndDrop = (function() {
    var fn;
    var ships         = document.querySelectorAll("[draggable]");
    var drop          = document.querySelector(".drop");
    var shipPositions = [];
    var gameField     = {};

    /**
     * start this module
     */
    function init() {
        listener();
    }

    /**
     * load the other modules of the app
     */
    function loadModules() {
        fn = sinkOrSwim.functions;

        init();
    }

    /**
     * listen to the drag&drop events
     */
    function listener() {
        Array.forEach(ships, function(ship) {
            fn.addEvent(ship, "dragstart", handleDragStart);
        });
        fn.addEvent(drop, "dragover", handleDragOver);
        fn.addEvent(drop, "drop", handleDrop);
    }

    /**
     * throw an error in the console and show user a message
     * @param {string} error a string with the error message
     */
    function throwError(error) {
        console.error(error);
        fn.getById("dragAndDropError").innerHTML = error;
    }

    /**
     * remove the error message for the user
     */
    function removeError() {
        fn.getById("dragAndDropError").innerHTML = "";
    }

    /**
     * start the dragAndDrop movement
     * @param {Object} e events
     */
    function handleDragStart(e) {
        var _me         = e.target;
        var _actCounter = _me.parentNode.getAttribute("data-counter");
        _actCounter     = parseInt(_actCounter);

        if(_actCounter <= 0) {
            return;
        }

        removeError();
        e.dataTransfer.setData("text", _me.id);
    }

    /**
     * functions when element is moving
     * @param {Object} e event
     */
    function handleDragOver(e) {
        e.preventDefault();
    }

    /**
     * drop the ship on the gamefield
     * @param {Object} e event
     */
    function handleDrop(e) {
        e.preventDefault();

        var cell         = e.target;
        gameField.elem   = cell.parentNode;
        gameField.height = gameField.elem.getAttribute("data-height");
        gameField.width  = gameField.elem.getAttribute("data-width");

        var data = e.dataTransfer.getData("text");
        var ship = new Ship(fn.getById(data), cell);

        ship.checkGameBorder();
        ship.checkPosition();
        ship.appendShip(cell);
        ship.updateCounter();
    }

    /**
     * Ship-object for the moving objects
     * @param {Element} shipElem the ship
     * @param {Element} cellTarget the cell where the ship should be dropped
     * @constructor
     */
    function Ship(shipElem, cellTarget) {
        this.ship     = shipElem;
        this.size     = parseInt(this.ship.getAttribute("data-size"));
        this.hasError = false;
        this.startX   = parseInt(cellTarget.getAttribute("data-x"));
        this.startY   = parseInt(cellTarget.getAttribute("data-y"));
        this.tempYmax = 0;
        this.tempXmax = 0;
    }

    /**
     * check if the ship the gamefield
     * @method
     * @methodOf Ship
     */
    Ship.prototype.checkGameBorder = function() {
        if(this.ship.getAttribute("data-vertical") === "true") {
            if(gameField.height - this.startY < this.size) {
                this.hasError = true;
            } else {
                this.tempYmax = this.startY + this.size - 1;
                this.tempXmax = this.startX;
            }
        } else {
            if(gameField.width - this.startX < this.size) {
                this.hasError = true;
            } else {
                this.tempYmax = this.startY;
                this.tempXmax = this.startX + this.size - 1;
            }
        }

        if(this.hasError) {
            throwError("Ship outside of Border");
        }
    };

    /**
     * check if the ship is crossing with other ships
     * @method
     * @methodOf Ship
     */
    Ship.prototype.checkPosition = function() {
        var _tempPosArray = [];
        if(!this.hasError) {
            for(var x = this.startX; x <= this.tempXmax; x++) {
                for(var y = this.startY; y <= this.tempYmax; y++) {
                    _tempPosArray.push(x + "-" + y);
                }
            }

            for(var i = 0; i < _tempPosArray.length; i++) {
                if(shipPositions.indexOf(_tempPosArray[i]) !== -1) {
                    this.hasError = true;
                    throwError("Ship crossing with other ship");
                    break;
                }
            }
        }
    };

    /**
     * append the ship to the gamefield
     * @method
     * @methodOf Ship
     * @param {HTMLElement} cellTarget the cell where the ship will be dropped
     */
    Ship.prototype.appendShip = function(cellTarget) {
        if(!this.hasError) {
            var _yMax                 = this.tempYmax + 1;
            var _xMax                 = this.tempXmax + 1;
            this._shipParentContainer = this.ship.parentNode;

            for(var x = this.startX - 1; x <= _xMax; x++) {
                for(var y = this.startY - 1; y <= _yMax; y++) {
                    shipPositions.push(x + "-" + y);
                }
            }

            fn.addChild(this.ship, cellTarget);
        }
    };

    /**
     * update the counter of dropable ship of the same class
     * @method
     * @methodOf Ship
     */
    Ship.prototype.updateCounter = function() {
        if(!this.hasError) {
            var _actCounter = this._shipParentContainer.getAttribute("data-counter");
            var _newCounter = (_actCounter - 1).toString();

            this._shipParentContainer.setAttribute("data-counter", _newCounter);
            fn.query("span", this._shipParentContainer).innerHTML = _newCounter + "x";
        }
    };

    return {
        loadModules: loadModules
    };
})();