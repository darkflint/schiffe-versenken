"use strict";

sinkOrSwim.chat = (function() {
    var fn;
    var socket;
    var chatBox, chatBoxDisplay, chatBoxUserList, userNameInput;

    /**
     * load the other modules of the app
     */
    function loadModules() {
        fn = sinkOrSwim.functions;

        init();
    }

    function init() {
        socket         = io();
        chatBox        = fn.getById("chatbox");
        chatBoxDisplay = fn.getByTag("ul", chatBox)[0];
        chatBoxUserList = fn.getByTag("aside", chatBox)[0];
        userNameInput  = fn.getByTag("input", chatBox)["userName"];

        /**
         * fill name input even if socket isn´t connected first
         */
        if(localStorage.getItem("userName")) {
            userNameInput.value = localStorage.getItem("userName");
        }

        /**
         * connected to socket.io, let´s the fun start!
         */
        socket.on("connected", function(data) {
            console.log("connected", data);

            /**
             * to prevent name changes if reconnecting
             */
            if(localStorage.getItem("userName")) {
                /**
                 * if already an individual name is saved, use it
                 */
                console.log("use saved name", localStorage.getItem("userName"));
                socket.emit("newUserName", localStorage.getItem("userName"));
                userNameInput.value = localStorage.getItem("userName");
            } else {
                if(sessionStorage.getItem("tmpUserName")) {
                    /**
                     * no individual name used until now, socket bug-reconnecting, use last given name
                     */
                    console.log("tmp name", sessionStorage.getItem("tmpUserName"))
                    userNameInput.value = sessionStorage.getItem("tmpUserName");
                } else {
                    /**
                     * first connection
                     */
                    console.log("first:", data.userName);
                    userNameInput.value = data.userName;
                    sessionStorage.setItem("tmpUserName", data.userName);
                    socket.emit("newUserName", data.userName);
                }
            }
        });

        /**
         * tell server that user is leaving, thus informing the other users
         */
        fn.addEvent(window, "beforeunload", function() {
            socket.emit("userLeaving");
        });

        listenForSendingMessages();
        listenForReceivingMessages();
        listenForUserName();
        updateUsers();
    }

    function updateUsers() {
        socket.on("addUser", function(user) {
            console.log("new user", user);
            var userLink = fn.create("span");
            userLink.innerHTML = user;

            fn.addChild(userLink, chatBoxUserList);
        });

        socket.on("changeName", function(oldName, newName) {
            console.log("changeName", oldName, newName);
        });
    }

    function listenForUserName() {
        fn.addEvent(userNameInput, "change", function() {
            localStorage.setItem("userName", this.value);

            socket.emit("newUserName", this.value);
        });
    }

    function listenForSendingMessages() {
        var _chatBoxButton = fn.query("button", chatBox);

        fn.addEvent(_chatBoxButton, "click", function() {
            var _sendMsg = new Message();

            _sendMsg.getLocalUser();
            _sendMsg.getLocalMessage();
            _sendMsg.sendMessage();
        });
    }

    function listenForReceivingMessages() {
        socket.on("broadcast_message", function(msgObj) {
            var _receivedMsg = new Message(msgObj);

            _receivedMsg.getLocalTime();
            _receivedMsg.getRemoteUser();
            _receivedMsg.getRemoteMessage();
            _receivedMsg.displayMessage();
        });
    }

    function Message(msgObj) {
        this.remoteMsgObj = msgObj;
    }

    Message.prototype.getLocalUser = function() {
        var _user = userNameInput.value;

        if(_user === "" || typeof _user === "undefined") {
            console.error("no user");
            this.user = false;
        } else {
            this.user = _user;
        }
    };

    Message.prototype.getLocalMessage = function() {
        var _msg = fn.getByTag("input", chatBox)["chatMessage"].value;

        if(_msg === "" || typeof _msg === "undefined") {
            console.error("no message");
            this.msg = false;
        } else {
            this.msg = _msg;
        }
    };

    Message.prototype.sendMessage = function() {
        if(this.user && this.msg) {
            socket.emit("send_message", {
                user: this.user,
                msg : this.msg
            });
        } else {
            console.error("couldn't send message");
        }
    };

    Message.prototype.getLocalTime = function() {
        var _localDateString, _localTimeString = "";
        var _dateObj         = new Date(this.remoteMsgObj.timestamp);

        if(new Date().toLocaleDateString()) {
            _localDateString = _dateObj.toLocaleDateString();
        } else {
            var _day   = _dateObj.getDate();
            var _month = _dateObj.getMonth() + 1;
            var _year  = _dateObj.getFullYear();

            _localDateString = _day + "." + _month + "." + _year;
        }

        if(new Date().toLocaleTimeString()) {
            _localTimeString = _dateObj.toLocaleTimeString();
        } else {
            var _hour = _dateObj.getHours();
            var _minutes = _dateObj.getMinutes();
            var _seconds = _dateObj.getSeconds();

            _localTimeString = _hour + ":" + _minutes + ":" + _seconds;
        }

        this.localDate = _localDateString;
        this.localTime = _localTimeString;
    };

    Message.prototype.getRemoteUser = function() {
        this.user = this.remoteMsgObj.user;
    };

    Message.prototype.getRemoteMessage = function() {
        this.msg = this.remoteMsgObj.msg;
    };

    Message.prototype.displayMessage = function() {
        var _li       = fn.create("li");
        var _spanTime = fn.create("span");
        var _spanUser = fn.create("span");
        var _spanMsg  = fn.create("span");

        _spanTime.innerHTML = this.localDate + " " + this.localTime;
        _spanUser.innerHTML = this.user + ":";
        _spanMsg.innerHTML  = this.msg;

        fn.addChild(_spanTime, _li);
        fn.addChild(_spanUser, _li);
        fn.addChild(_spanMsg, _li);

        fn.addChild(_li, chatBoxDisplay);
    };

    return {
        loadModules: loadModules
    };
})();