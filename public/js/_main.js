"use strict";

sinkOrSwim.main = (function() {
    var fn;
    var dragAndDrop;

    /**
     * load the other modules of the app
     */
    function loadModules() {
        fn          = sinkOrSwim.functions;
        dragAndDrop = sinkOrSwim.dragAndDrop;
    }

    /**
     * start the app
     */
    function init() {
        //dragAndDrop.listenDragAndDrop();
    }

    return {
        loadModules: loadModules
    }
})();