"use strict";

/**
 *
 * @type {{loadModules, addChild, addEvent, getById, query, queryAll}}
 */
sinkOrSwim.functions = (function() {

    /**
     * load the other modules of the app
     */
    function loadModules() {
    }

    /**
     * alias for elem.appendChild(child)
     * @param {Element} child
     * @param {Element} parent
     */
    function addChild(child, parent) {
        if(parent === null || typeof parent === "undefined") {
            parent = document;
        }

        parent.appendChild(child);
    }

    /**
     * alias for elem.addEventListener(event, callback, useCapture)
     * @param {Element} elem
     * @param {String} event
     * @param {Function} fn
     * @param {Boolean|Optional} useCapture A Boolean that indicates that events of this type will be dispatched to the registered listener before being dispatched to any EventTarget beneath it in the DOM tree. Events that are bubbling upward through the tree will not trigger a listener designated to use capture. Event bubbling and capturing are two ways of propagating events that occur in an element that is nested within another element, when both elements have registered a handle for that event. The event propagation mode determines the order in which elements receive the event. See DOM Level 3 Events and JavaScript Event order for a detailed explanation. If not specified, useCapture defaults to false.
     */
    function addEvent(elem, event, fn, useCapture) {
        if(typeof useCapture === "undefined") {
            useCapture = false;
        }

        elem.addEventListener(event, fn, useCapture);
    }

    /**
     * Alias for elem.getElementById(id)
     * @param {String} id
     * @param elem
     * @returns {Element}
     */
    function getById(id, elem) {
        if(elem === null || typeof elem === "undefined") {
            elem = document;
        }

        return elem.getElementById(id);
    }

    /**
     * Alias for elem.getElementsByTagName(tag)
     * @param tag
     * @param elem
     * @returns {NodeList}
     */
    function getByTag(tag, elem) {
        if(elem === null || typeof elem === "undefined") {
            elem = document;
        }

        return elem.getElementsByTagName(tag);
    }

    /**
     * Alias for document.createElement(elem)
     * @param elem
     * @returns {Element}
     */
    function create(elem) {
        return document.createElement(elem);
    }

    /**
     * Alias for elem.querySelector(selector)
     * @param {String} selector
     * @param elem
     * @returns {Element}
     */
    function query(selector, elem) {
        if(elem === null || typeof elem === "undefined") {
            elem = document;
        }

        return elem.querySelector(selector);
    }


    /**
     * Alias for elem.querySelectorAll(selector)
     * @param {String} selector
     * @param elem
     * @returns {NodeList}
     */
    function queryAll(selector, elem) {
        if(elem === null || typeof elem === "undefined") {
            elem = document;
        }

        return elem.querySelectorAll(selector);
    }

    return {
        loadModules: loadModules,
        addChild   : addChild,
        addEvent   : addEvent,
        create     : create,
        getById    : getById,
        getByTag   : getByTag,
        query      : query,
        queryAll   : queryAll
    };
})();