"use strict";

var sinkOrSwim = {};

window.addEventListener("load", function() {
    var _scriptArray = ["functions", "main", "chat"];

    switch(document.querySelector("main").id) {
        case "game":
            _scriptArray.push("dragAndDrop");
            break;
    }

    /**
     * load the scripts
     */
    _loadScripts();

    /**
     * create the scripts elements after loading the size and add them to body
     * also start the initializer after all scripts are loaded
     */
    function _loadScripts() {
        /**
         * number of scripts finished loading
         * @type {number}
         * @private
         */
        var _scriptLoaded = 0;

        if(Array.isArray(_scriptArray)) {
            /**
             * iterate all scripts and add them to body
             */
            _scriptArray.forEach(function(element) {
                var _script = document.createElement("script");
                _script.src = "/js/_" + element + ".js";
                document.body.appendChild(_script);

                /**
                 * wait until all scripts are loaded...
                 */
                _script.addEventListener("load", function() {
                    _scriptLoaded++;

                    /**
                     * ... and then make them all available in all files
                     */
                    if(_scriptLoaded === _scriptArray.length) {
                        _scriptArray.forEach(function(module) {
                            sinkOrSwim[module].loadModules();
                        });
                    }
                });
            });
        }
    }
});