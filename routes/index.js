var express = require('express');
var router  = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.render('_index', {
        title    : "Schiffe versenken",
        sectionId: "home",
        modes    : [
            {
                id   : 1,
                value: "classic"
            }
        ]
    });
});

router.post("/game", function(req, res) {
    console.log("start game;", req.body);

    res.render("_game", {
        title    : "Game on!",
        sectionId: "game",
        game     : {
            field: {
                width : 10,
                height: 10
            },
            ships: [
                {
                    name  : "battleship",
                    amount: 1,
                    size  : 5
                },
                {
                    name  : "cruiser",
                    amount: 2,
                    size  : 4
                },
                {
                    name  : "destroyer",
                    amount: 3,
                    size  : 3
                },
                {
                    name  : "submarine",
                    amount: 4,
                    size  : 2
                }
            ]
        }
    });
});

router.get("*", function(req, res) {
    res.render("error", {
        sectionId: "error",
        message  : "Page not found",
        error    : {
            status: 404
        }
    });
});

module.exports = router;
